-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 13, 2020 at 06:56 AM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.2.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `datasiswa`
--

-- --------------------------------------------------------

--
-- Table structure for table `kecamatan`
--

CREATE TABLE `kecamatan` (
  `id_kecamatan` int(11) NOT NULL,
  `id_kota_kabupaten` int(11) NOT NULL,
  `kecamatan` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `kecamatan`
--

INSERT INTO `kecamatan` (`id_kecamatan`, `id_kota_kabupaten`, `kecamatan`) VALUES
(1, 1, 'Bandung Timur'),
(2, 2, 'Cimahi Utara'),
(3, 4, 'Padalarang'),
(4, 2, 'Cimahi Tengah'),
(5, 1, 'Antapani'),
(6, 4, 'Lembang'),
(7, 2, 'Cimahi Selatan'),
(9, 4, 'Batu Jajar');

-- --------------------------------------------------------

--
-- Table structure for table `kota_kabupaten`
--

CREATE TABLE `kota_kabupaten` (
  `id_kota_kabupaten` int(11) NOT NULL,
  `kota_kabupaten` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `kota_kabupaten`
--

INSERT INTO `kota_kabupaten` (`id_kota_kabupaten`, `kota_kabupaten`) VALUES
(1, 'Kota Bandung'),
(2, 'Kota Cimahi'),
(4, 'Kab. Bandung Barat');

-- --------------------------------------------------------

--
-- Table structure for table `siswa`
--

CREATE TABLE `siswa` (
  `id_siswa` int(11) NOT NULL,
  `id_kecamatan` int(11) NOT NULL,
  `id_kota_kabupaten` int(11) NOT NULL,
  `nama_siswa` varchar(50) NOT NULL,
  `alamat` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `siswa`
--

INSERT INTO `siswa` (`id_siswa`, `id_kecamatan`, `id_kota_kabupaten`, `nama_siswa`, `alamat`) VALUES
(1, 1, 1, 'Agus', 'Alamat Siswa'),
(2, 2, 2, 'Budi', 'Alamat Siswa'),
(3, 3, 4, 'Nana', 'Alamat Siswa'),
(4, 3, 4, 'Bambang', 'Alamat Siswa'),
(5, 4, 2, 'Fitri', 'Alamat Siswa'),
(6, 5, 1, 'Bagus', 'Alamat Siswa'),
(7, 5, 1, 'Hartoko', 'Alamat Siswa'),
(8, 6, 4, 'Dadan ', 'Alamat Siswa'),
(9, 7, 2, 'Ceceng', 'Alamat Siswa'),
(10, 1, 1, 'Ilham', 'Alamat Siswa'),
(11, 9, 4, 'Iqbal', 'Alamat Siswa'),
(12, 4, 2, 'Adi', 'Alamat Siswa');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `kecamatan`
--
ALTER TABLE `kecamatan`
  ADD PRIMARY KEY (`id_kecamatan`);

--
-- Indexes for table `kota_kabupaten`
--
ALTER TABLE `kota_kabupaten`
  ADD PRIMARY KEY (`id_kota_kabupaten`);

--
-- Indexes for table `siswa`
--
ALTER TABLE `siswa`
  ADD PRIMARY KEY (`id_siswa`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `kecamatan`
--
ALTER TABLE `kecamatan`
  MODIFY `id_kecamatan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `kota_kabupaten`
--
ALTER TABLE `kota_kabupaten`
  MODIFY `id_kota_kabupaten` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `siswa`
--
ALTER TABLE `siswa`
  MODIFY `id_siswa` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
