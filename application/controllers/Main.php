<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Main extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('ModelSiswa');	
		$this->load->helper('url');	
		$this->load->library('curl');	
	}

	public function index()
	{
		$data['siswa'] = $this->ModelSiswa->get();
		$this->load->view('siswa/home', $data);
	}

	public function getDetail($id)
	{
	
		$data['siswa'] = $this->ModelSiswa->getDetail($id);
		$data['kota_kabupaten'] = $this->ModelSiswa->getKotaKabupaten();
	
		$data['kecamatan'] = $this->ModelSiswa->getKecamatan();
		// print_r($data['kecamatan']);die;
		// $data['kota_kabupaten'] = $this->ModelSiswa->getKotaKabupaten();
		$this->load->view('siswa/edit', $data);
	}

	public function form_tambah()
	{
		$data['kota_kabupaten'] = $this->ModelSiswa->getKotaKabupaten();
		// $data['kecamatan'] = $this->ModelSiswa->getKecamatan();
		$this->load->view('siswa/tambah',$data);
	}

	public function create()
	{
		$siswa = array(
			'nama_siswa' => $this->input->post('nama_siswa'), 
			'id_kota_kabupaten' => $this->input->post('id_kota_kabupaten'), 
			'id_kecamatan' => $this->input->post('id_kecamatan'), 
			'alamat' => $this->input->post('alamat'), 
		);
		$this->ModelSiswa->create($siswa);
		redirect('main');
	}

	public function update()
	{
		$siswa = array(
			'nama_siswa' => $this->input->post('nama_siswa'), 
			'id_kota_kabupaten' => $this->input->post('id_kota_kabupaten'), 
			'id_kecamatan' => $this->input->post('id_kecamatan'), 
			'alamat' => $this->input->post('alamat'), 
		);
		// print_r($siswa);die;
		$id = $this->input->post('id_siswa');
		$this->ModelSiswa->update($siswa, $id);
		redirect('main');
	}

	public function delete($id)
	{
		$this->ModelSiswa->delete($id);
		redirect('main');
	}

	public function daftarKecamatan(){
    // Ambil data ID Provinsi yang dikirim via ajax post
    $id_kota_kabupaten = $this->input->post('id_kota_kabupaten');
    
    $kecamatan = $this->ModelSiswa->getKecamatanByIdKota($id_kota_kabupaten);
    
    // Buat variabel untuk menampung tag-tag option nya
    // Set defaultnya dengan tag option Pilih
    $daftar = "<option value=''>Pilih</option>";
    
    foreach($kecamatan as $key){
      $daftar .= "<option value='".$key->id_kecamatan."'>".$key->kecamatan."</option>"; // Tambahkan tag option ke variabel $lists
    }
    
    $callback = array('daftar_kecamatan'=>$daftar); // Masukan variabel lists tadi ke dalam array $callback dengan index array : list_kota
    echo json_encode($callback); // konversi varibael $callback menjadi JSON
	}


	// ============================================Kecamatan========================================
	public function index_kecamatan()
	{
		$data['kecamatan'] = $this->ModelSiswa->getKecamatan();
		$this->load->view('kecamatan/home_kecamatan', $data);
	}

	public function getDetail_kecamatan($id)
	{
	
		$data['kecamatan'] = $this->ModelSiswa->getDetailKecamatan($id);
		// print_r($data['kecamatan']);die;
		$data['kota_kabupaten'] = $this->ModelSiswa->getKotaKabupaten();
		$this->load->view('kecamatan/edit_kecamatan', $data);
	}

	public function form_tambah_kecamatan()
	{
		$data['kota_kabupaten'] = $this->ModelSiswa->getKotaKabupaten();
		$this->load->view('kecamatan/tambah_kecamatan',$data);
	}

	public function create_kecamatan()
	{
		$kecamatan = array(
			'kecamatan' => $this->input->post('kecamatan'), 
			'id_kota_kabupaten' => $this->input->post('id_kota_kabupaten'), 
		);
		$this->ModelSiswa->createKecamatan($kecamatan);
		redirect('main/index_kecamatan');
	}

	public function update_kecamatan()
	{
		$kecamatan = array(
			'kecamatan' => $this->input->post('kecamatan'), 
			'id_kota_kabupaten' => $this->input->post('id_kota_kabupaten'), 
		);
		$id = $this->input->post('id');
		$this->ModelSiswa->updateKecamatan($kecamatan, $id);
		redirect('main/index_kecamatan');
	}

	public function delete_kecamatan($id)
	{
		$this->ModelSiswa->deleteKecamatan($id);
		redirect('main/index_kecamatan');
	}

	// =============================================Kota/Kaupaten=====================================

	public function index_kota_kabupaten()
	{
		$data['kota_kabupaten'] = $this->ModelSiswa->getKotaKabupaten();
		$this->load->view('kota_kabupaten/home_kota_kabupaten', $data);
	}

	public function getDetail_kota_kabupaten($id)
	{
	
		$data['kota_kabupaten'] = $this->ModelSiswa->getDetailKotaKabupaten($id);
		$this->load->view('kota_kabupaten/edit_kota_kabupaten', $data);
	}

	public function form_tambah_kota_kabupaten()
	{
		$this->load->view('kota_kabupaten/tambah_kota_kabupaten');
	}

	public function create_kota_kabupaten()
	{
		$kota_kabupaten = array(
			'kota_kabupaten' => $this->input->post('kota_kabupaten'), 
		);
		$this->ModelSiswa->createKotaKabupaten($kota_kabupaten);
		redirect('main/index_kota_kabupaten');
	}

	public function update_kota_kabupaten()
	{
		$kota_kabupaten = array(
			'kota_kabupaten' => $this->input->post('kota_kabupaten'), 
		);

		$id = $this->input->post('id');
		$this->ModelSiswa->updateKotaKabupaten($kota_kabupaten, $id);
		redirect('main/index_kota_kabupaten');
	}

	public function delete_kota_kabupaten($id)
	{
		$this->ModelSiswa->deleteKotaKabupaten($id);
		redirect('main/index_kota_kabupaten');
	}

	public function view()
	{
		$data['kota_kabupaten'] = $this->ModelSiswa->getKotaKabupaten();
		$this->load->view('view_wilayah',$data);
	}

	public function list_kecamatan(){
        $id_kota = $this->input->post('id_kota_kabupaten');
        $kota = $this->ModelSiswa->get_list_kecamatan($id_kota);
        $lists = "<option value=''>--Please-Select---</option>";
        foreach($kota as $data){
            $lists .= "<option value='".$data->id_kecamatan."'>".$data->kecamatan."</option>";
        }
        $callback = array('list_kota'=>$lists);
        echo json_encode($callback);
    }
}
