<?php
class ModelSiswa extends CI_Model{

	public function __construct(){
		$this->load->database();
	}
	// ========================================Siswa========================================

	public function get()
	{
		$this->db->join('kota_kabupaten','siswa.id_kota_kabupaten = kota_kabupaten.id_kota_kabupaten');
		$this->db->join('kecamatan','siswa.id_kecamatan = kecamatan.id_kecamatan');
		$this->db->select('kota_kabupaten.kota_kabupaten, kota_kabupaten.id_kota_kabupaten as id_kota, kecamatan.kecamatan, kecamatan.id_kecamatan as id_kec, siswa.*');
		return $this->db->get('siswa')->result();
	}

	public function getDetail($id)
	{
		$this->db->join('kota_kabupaten','siswa.id_kota_kabupaten = kota_kabupaten.id_kota_kabupaten');
		$this->db->join('kecamatan','siswa.id_kecamatan = kecamatan.id_kecamatan');
		$this->db->select('kota_kabupaten.kota_kabupaten, kota_kabupaten.id_kota_kabupaten as id_kota, kecamatan.kecamatan, kecamatan.id_kecamatan as id_kec, siswa.*');
		return $this->db->get_where('siswa', array('id_siswa' => $id))->result();
	}

	public function create($siswa)
	{
		return $this->db->insert('siswa', $siswa);
	}

	public function update($siswa,$id)
	{
		return $this->db->update('siswa', $siswa, array('id_siswa' => $id));
	}

	public function delete($id)
	{
		$this->db->where('id_siswa',$id);
		$this->db->delete('siswa');
	}

	// ========================================Kecamatan========================================

	public function getKecamatan()
	{
		$this->db->join('kota_kabupaten','kecamatan.id_kota_kabupaten = kota_kabupaten.id_kota_kabupaten');
		$this->db->select('kota_kabupaten.kota_kabupaten, kecamatan.*');
		return $this->db->get('kecamatan')->result();
	}

	public function getDetailKecamatan($id)
	{
		$this->db->join('kota_kabupaten','kecamatan.id_kota_kabupaten = kota_kabupaten.id_kota_kabupaten');
		$this->db->select('kota_kabupaten.kota_kabupaten, kecamatan.*');
		return $this->db->get_where('kecamatan', array('id_kecamatan' => $id))->result();
	}

	public function getKecamatanByIdKota($id)
	{
		return $this->db->get_where('kecamatan', array('id_kota_kabupaten' => $id))->result();
	}

	public function createKecamatan($kecamatan)
	{
		return $this->db->insert('kecamatan', $kecamatan);
	}

	public function updateKecamatan($kecamatan,$id)
	{
		return $this->db->update('kecamatan', $kecamatan, array('id_kecamatan' => $id));
	}

	public function deleteKecamatan($id)
	{
		$this->db->where('id_kecamatan',$id);
		$this->db->delete('kecamatan');
	}

	// ========================================Kabupaten================================================

	public function getKotaKabupaten()
	{
		return $this->db->get('kota_kabupaten')->result();
	}

	public function getDetailKotaKabupaten($id)
	{
		return $this->db->get_where('kota_kabupaten', array('id_kota_kabupaten' => $id))->result();
	}

	public function createKotaKabupaten($kota_kabupaten)
	{
		return $this->db->insert('kota_kabupaten', $kota_kabupaten);
	}

	public function updateKotaKabupaten($kota_kabupaten,$id)
	{
		return $this->db->update('kota_kabupaten', $kota_kabupaten, array('id_kota_kabupaten' => $id));
	}

	public function deleteKotaKabupaten($id)
	{
		$this->db->where('id_kota_kabupaten',$id);
		$this->db->delete('kota_kabupaten');
	}


	function get_list_kota() {
        return $this->db->get('kecamatan')->result(); // Tampilkan semua data yang ada di tabel provinsi
    }

    function get_list_kecamatan($id_kota){
        $this->db->where('id_kota_kabupaten', $id_kota);
        $result = $this->db->get('kecamatan')->result();
        return $result; 
    }
}