<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>
	<a href="<?php echo base_url().'main/form_tambah';?>">Tambah Siswa</a>
	<table border="1">
		<thead>
			<tr>
				<th>No. </th>
				<th>Nama Siswa </th>
				<th>Kota/Kabupaten </th>
				<th>Kecamatan</th>
				<th>Alamat</th>
				<th>Aksi</th>
			</tr>
		</thead>
		<?php
		$no = 0;
		foreach ($siswa as $key) {
		$no++;?>
		<tbody>
			<tr>
				<td><?php echo $no;?></td>
				<td><?php echo $key->nama;?></td>
				<td><?php echo $key->id_kota_kabupaten;?></td>
				<td><?php echo $key->id_kecamatan;?></td>
				<td><?php echo $key->alamat;?></td>
				<td>
					<a href="<?php echo base_url('main/getdetail/'.$key->id);?>">Update</a>
					<a href="<?php echo base_url('main/delete/'.$key->id);?>">Delete</a>
				</td>
			</tr>
		</tbody>
	<?php } ?>
	</table>

</body>
</html>