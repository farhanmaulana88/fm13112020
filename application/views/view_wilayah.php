<html>
<head>
    <title>Chained Dropdown</title>
</head>
<body>
    <h1>Chained Dropdown</h1>
    <hr>
    <table cellpadding="8">
        <tr>
            <td><b>kota_kabupaten</b></td>
            <td>
                <select name="kota_kabupaten" id="kota_kabupaten" style="width: 200px;">
                    <option value="">--Please-select---</option>
                    <?php foreach ($kota_kabupaten as $data){ 
                            echo "<option value='".$data->id_kota_kabupaten."'>".$data->kota_kabupaten."</option>";
                        }
                    ?>
                </select>
            </td>
        </tr>
        <tr>
            <td><b>Kota</b></td>
            <td>
                <select name="kota" id="kecamatan" style="width: 200px;">
                    <option value="">--Please-select---</option>
                </select>
                <div id="loading" style="margin-top: 15px;">
                    <img src="<?php echo base_url();?>assets/images/loading.gif" width="18"> <small>Loading...</small>
                </div>
            </td>
        </tr>
    </table>
	
    <!-- Load library/plugin jquery nya -->
    <script src="<?php echo base_url('assets/js/jquery.min.js'); ?>" type="text/javascript"></script>
    <script>
        $(document).ready(function(){ // Ketika halaman sudah siap (sudah selesai di load)
            // Kita sembunyikan dulu untuk loadingnya
            $("#loading").hide();
 
            $("#kota_kabupaten").change(function(){ // Ketika user mengganti atau memilih data kota_kabupaten
                $("#kecamatan").hide(); // Sembunyikan dulu combobox kecamatan nya
                $("#loading").show(); // Tampilkan loadingnya
 
                $.ajax({
                    type: "POST", // Method pengiriman data bisa dengan GET atau POST
                    url: "<?php echo base_url("wilayah/list_kota"); ?>", // Isi dengan url/path file php yang dituju
                    data: {id_provinsi : $("#kota_kabupaten").val()}, // data yang akan dikirim ke file yang dituju
                    dataType: "json",
                    beforeSend: function(e) {
                        if(e && e.overrideMimeType) {
                                e.overrideMimeType("application/json;charset=UTF-8");
                        }
                    },
                    success: function(response){ // Ketika proses pengiriman berhasil
                        $("#loading").hide(); // Sembunyikan loadingnya
 
                        // set isi dari combobox kecamatan
                        // lalu munculkan kembali combobox kotanya
                        $("#kecamatan").html(response.list_kota).show();
                    },
                    error: function (xhr, ajaxOptions, thrownError) { // Ketika ada error
                        alert(xhr.status + "\n" + xhr.responseText + "\n" + thrownError); // Munculkan alert error
                    }
                });
            });
        });
    </script>
</body>
</html>