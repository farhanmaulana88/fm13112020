<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>
	<a href="<?php echo base_url().'main/form_tambah';?>">Tambah Siswa</a>
	<table border="1">
		<thead>
			<tr>
				<th>No. </th>
				<th>Nama Siswa </th>
				<th>Kota/Kabupaten </th>
				<th>Kecamatan</th>
				<th>Alamat</th>
				<th>Aksi</th>
			</tr>
		</thead>
		<?php
		$no = 0;
		foreach ($siswa as $key) {
		$no++;?>
		<tbody>
			<tr>
				<td><?php echo $no;?></td>
				<td><?php echo $key->nama_siswa;?></td>
				<td><?php echo $key->kota_kabupaten;?></td>
				<td><?php echo $key->kecamatan;?></td>
				<td><?php echo $key->alamat;?></td>
				<td>
					<a href="<?php echo base_url('main/getdetail/'.$key->id_siswa);?>">Update</a>
					<a href="<?php echo base_url('main/delete/'.$key->id_siswa);?>">Delete</a>
				</td>
			</tr>
		</tbody>
	<?php } ?>
	</table>
	<br><br><br>
	<a href="<?php echo base_url('main/index_kota_kabupaten');?>"><h3>Kelola Kota/Kabupaten</h3></a>
	<a href="<?php echo base_url('main/index_kecamatan');?>"><h3>Kelola Kecamatan</h3></a>


</body>
</html>