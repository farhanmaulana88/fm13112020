<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>
	<h2>Update Siswa</h2>
	<form method="post" action="<?php echo base_url().'main/update';?>">
		<?php foreach ($siswa as $key) { ?>
			<input type="hidden" name="id_siswa" value="<?php echo $key->id_siswa;?>">
			Nama : <input type="text" name="nama_siswa" value="<?php echo $key->nama_siswa;?>"><br>
			Kota/Kabupaten : <select name="id_kota_kabupaten">
									<option selected value="<?php echo $key->id_kota;?>"><?php echo $key->kota_kabupaten;?></option>
                                <?php foreach ($kota_kabupaten as $main) { ?>
                                    <option value="<?php echo $main->id_kota_kabupaten;?>"><?php echo $main->kota_kabupaten;?></option>
                                <?php } ?>
                            </select><br>
        Kecamatan : <select name="id_kecamatan">
									<option selected value="<?php echo $key->id_kecamatan;?>"><?php echo $key->kecamatan;?></option>
                                <?php foreach ($kecamatan as $value) { ?>
                                    <option value="<?php echo $value->id_kecamatan;?>"><?php echo $value->kecamatan;?></option>
                                <?php } ?>
                            </select><br>
        Alamat : <textarea name="alamat" value="<?php echo $key->alamat;?>"><?php echo $key->alamat;?></textarea>
            	
		<?php } ?>

		<button type="submit">Update</button>
	</form>

</body>
</html>