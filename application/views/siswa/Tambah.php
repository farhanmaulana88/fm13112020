<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>
	<h2>Tambah Siswa</h2>
	<form method="post" action="<?php echo base_url().'main/create';?>">
			Nama Siswa : <input type="text" name="nama_siswa"><br>
			Kota/Kabupaten : <select name="id_kota_kabupaten" id="kota_kabupaten" style="width: 200px;">
                    <option value="">--Please-select---</option>
                    <?php foreach ($kota_kabupaten as $data){ 
                            echo "<option value='".$data->id_kota_kabupaten."'>".$data->kota_kabupaten."</option>";
                        }
                    ?>
                </select><br>

                Kecamatan : <select name="id_kecamatan" id="kecamatan" style="width: 200px;">
                    <option value="">--Please-select---</option>
                </select><br>
                Alamat : <textarea name="alamat"></textarea><br>

			<button type="submit">Tambah</button>
	</form>

	 <script src="<?php echo base_url('assets/js/jquery.min.js'); ?>" type="text/javascript"></script>
    <script>
        $(document).ready(function(){
            $("#loading").hide();
 
            $("#kota_kabupaten").change(function(){
                $("#kecamatan").hide();
                $("#loading").show(); 
 
                $.ajax({
                    type: "POST", 
                    url: "<?php echo base_url("main/list_kecamatan"); ?>", 
                    data: {id_kota_kabupaten : $("#kota_kabupaten").val()},
                    dataType: "json",
                    beforeSend: function(e) {
                        if(e && e.overrideMimeType) {
                                e.overrideMimeType("application/json;charset=UTF-8");
                        }
                    },
                    success: function(response){
                        $("#loading").hide(); 
                        $("#kecamatan").html(response.list_kota).show();
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        alert(xhr.status + "\n" + xhr.responseText + "\n" + thrownError);
                    }
                });
            });
        });
    </script>
</body>
</html>